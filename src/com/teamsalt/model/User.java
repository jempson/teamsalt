package com.teamsalt.model;

import java.util.List;

import com.jfinal.plugin.activerecord.Model;
import com.teamsalt.kit.HtmlTagKit;

public class User extends Model<User> {
	public static final User dao = new User();

	public static final String tableName = "user";

	public static final String uuid = "uuid";
	public static final String username = "username";
	public static final String email = "email";
	public static final String password = "password";
	public static final String description = "description";
	public static final String createTime = "create_time";
	public static final String avatar = "avatar";// 头像 

	public User getByUuid(String uuid) {
		return dao.findById(uuid);
	}

	public User login(String email, String password) {
		return dao.findFirst("select * from user where email=? and password=?",
				email, password);
	}

	public void myUpdate() {
		HtmlTagKit.htmlEncode(this);
		this.update();
	}

	/**
	 * 
	 * @Title: getUsersByProjectUuid
	 * @Description: 获得项目关联的用户
	 * @param @param projectUuid
	 * @param @return 设定文件
	 * @return List<Users> 返回类型
	 * @throws
	 */
	public List<User> getUsersByProjectUuid(String projectUuid) {
		String sql = "SELECT * FROM  user t1, project_user t2 WHERE t1.uuid = t2.user_uuid and t2.project_uuid = ?";
		return dao.find(sql, projectUuid);
	}

	public User getUserByEmail(String email) {
		return dao.findFirst("select * from user where email=?", email);
	}

	public List getAllUser() {
		return dao.find("select * from user");
	}

	public List getProjectUsers(String pid) {
		return dao
				.find("select t2.* from project_user t1,user t2 where t1.user_uuid = t2.uuid and t1.project_uuid=?",
						pid);
	}

	public User queryUserProjectByEmail(String projectUuid, String email) {
		String sql = "select t2.* from project_user t1,user t2 where t1.user_uuid = t2.uuid and t1.project_uuid=? and t2.email=?";
		return dao.findFirst(sql, projectUuid, email);
	}
}
