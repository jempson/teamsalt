package com.teamsalt.model;

import com.jfinal.plugin.activerecord.Model;

public class ProjectUser extends Model<ProjectUser> {
	public static final ProjectUser dao = new ProjectUser();
	public static final String tableName = "project_user";
	public static final String projectUuid = "project_uuid";
	public static final String userUuid = "user_uuid";
	public static final String isAdmin = "is_admin"; // 0 普通用户 1 管理员
	public static final int isAdmin_admin = 1; // 1 管理员
	public static final int isAdmin_member = 0; // 0 普通用户

	/**
	 * 判断用户是否存在于项目中
	 * 
	 * @param projectUuid
	 * @param userUuid
	 * @return
	 */
	public ProjectUser queryProjectUserByEmail(String projectUuid,
			String userUuid) {
		String sql = "select t1.* from project_user t1 where t1.user_uuid = ? and t1.project_uuid=? ";
		return dao.findFirst(sql, userUuid, projectUuid);
	}
}
