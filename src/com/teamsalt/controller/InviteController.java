package com.teamsalt.controller;

import java.sql.Timestamp;
import java.util.Date;

import org.apache.log4j.Logger;

import com.jfinal.aop.Before;
import com.jfinal.aop.ClearInterceptor;
import com.jfinal.aop.ClearLayer;
import com.jfinal.core.Controller;
import com.jfinal.ext.interceptor.GET;
import com.teamsalt.common.TeamsaltConstants;
import com.teamsalt.kit.DateKit;
import com.teamsalt.kit.UuidKit;
import com.teamsalt.model.Invite;
import com.teamsalt.model.InviteUser;
import com.teamsalt.model.ProjectUser;
import com.teamsalt.model.User;

/**
 * 
 * @ClassName: InviteController
 * @Description: 用户邀请
 * @Author zhaopeng
 * @Email user.zhaopeng@gmail.com
 * @Date 2013-10-12 上午1:28:16
 * 
 */
public class InviteController extends Controller {
	private static final Logger log = Logger.getLogger(InviteController.class);

	@Before(GET.class)
	@ClearInterceptor(ClearLayer.ALL)
	public void index() {
		String code = getPara("code");
		Invite invite = Invite.dao.queryByCode(code);
		if (invite == null) {
			setAttr("message", "非法的邀请链接!");
			render("/invite/error.jsp");
			return;
		}
		if (invite.getInt(Invite.isValid) == Invite.isValid_invalid) {
			setAttr("message", "链接已经失效!");
			render("/invite/error.jsp");
			return;
		}
		if (!DateKit.compareDate(invite.getTimestamp(Invite.inviteOvertime)
				.toString(), DateKit.getNowDateTime())) {
			Invite updateInvite = new Invite();// 更新邀请CODE失效
			updateInvite.set(Invite.id, invite.get(Invite.id));
			updateInvite.set(Invite.isValid, Invite.isValid_invalid);
			updateInvite.update();

			setAttr("message", "邀请已经过期!");
			render("/invite/error.jsp");
			return;
		}
		User user = User.dao.getUserByEmail(invite.getStr(Invite.inviteeEmail));
		if (user == null) {// 用户是否在系统中已经存在,如此不存在,则跳转到注册页面
			// 这里本应跳转到注册页面,此处直接添加一个新用户
			setSessionAttr("invite", invite);// 将邀请信息存储到session中,在注册controller中判断,如果有邀请,则继续邀请关系流程,即:将用户加入到对应的项目中,
			// redirect("/register"); //此处需要跳转到注册页面,在实际使用中使用
			/* ------以下代码可以放在注册中---------- */
			user = new User();
			user.set(User.uuid, UuidKit.uuid());
			user.set(User.username, invite.get(Invite.inviteeName));
			user.set(User.password, "1");
			user.set(User.email, invite.get(Invite.inviteeEmail));
			user.set(User.createTime, new Date());
			user.save();
			Invite sessionInvite = getSessionAttr("invite");
			if (sessionInvite != null) {
				ProjectUser puser = new ProjectUser();// 加入到项目用户关系表中
				puser.set(ProjectUser.projectUuid,
						sessionInvite.get(Invite.projectUuid));
				puser.set(ProjectUser.userUuid, user.get(User.uuid));
				puser.set(ProjectUser.isAdmin, ProjectUser.isAdmin_member);
				puser.save();

				InviteUser inviteUser = new InviteUser();// 加入邀请用户关系表
				inviteUser.set(InviteUser.code, sessionInvite.get(Invite.code));
				inviteUser.set(InviteUser.inviterUuid,
						sessionInvite.get(Invite.inviterUuid));
				inviteUser.set(InviteUser.inviteeUuid, user.get(User.uuid));
				inviteUser.set(InviteUser.activeTime, new Date());
				inviteUser.set(InviteUser.projectUuid,
						sessionInvite.get(Invite.projectUuid));
				inviteUser.save();

				Invite updateInvite = new Invite();// 更新邀请CODE失效
				updateInvite.set(Invite.id, invite.get(Invite.id));
				updateInvite.set(Invite.isValid, Invite.isValid_invalid);
				updateInvite.update();

				setSessionAttr(TeamsaltConstants.LOGIN_USER, user);
				redirect("/home");
			}
			/* ---------------- */
			return;
		}

		ProjectUser projectUser = ProjectUser.dao.queryProjectUserByEmail(
				invite.getStr(Invite.projectUuid), user.getStr(User.uuid));
		if (projectUser == null) {// 判断是否已经存在于项目中,如果不存在,则加入到项目中,如果存在,则更新code失效
			ProjectUser puser = new ProjectUser();// 加入到项目用户关系表中
			puser.set(ProjectUser.projectUuid, invite.get(Invite.projectUuid));
			puser.set(ProjectUser.userUuid, user.get(User.uuid));
			puser.set(ProjectUser.isAdmin, ProjectUser.isAdmin_member);
			puser.save();

			InviteUser inviteUser = new InviteUser();// 加入邀请用户关系表
			inviteUser.set(InviteUser.code, invite.get(Invite.code));
			inviteUser.set(InviteUser.inviterUuid,
					invite.get(Invite.inviterUuid));
			inviteUser.set(InviteUser.inviteeUuid, user.get(User.uuid));
			inviteUser.set(InviteUser.activeTime, new Date());
			inviteUser.set(InviteUser.projectUuid,
					invite.get(Invite.projectUuid));
			inviteUser.save();

			Invite updateInvite = new Invite();// 更新邀请CODE失效
			updateInvite.set(Invite.id, invite.get(Invite.id));
			updateInvite.set(Invite.isValid, Invite.isValid_invalid);
			updateInvite.update();

			setSessionAttr(TeamsaltConstants.LOGIN_USER, user);
			redirect("/home");
			return;
		} else {
			Invite updateInvite = new Invite();// 更新邀请CODE失效
			updateInvite.set(Invite.id, invite.get(Invite.id));
			updateInvite.set(Invite.isValid, Invite.isValid_invalid);
			updateInvite.update();

			setAttr("message", "在项目中已经存在了!");
			render("/invite/error.jsp");
			return;
		}
	}

}
