package com.teamsalt.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.ext.interceptor.GET;
import com.jfinal.ext.interceptor.POST;
import com.teamsalt.common.Message;
import com.teamsalt.kit.ContextKit;
import com.teamsalt.kit.DateKit;
import com.teamsalt.kit.UuidKit;
import com.teamsalt.model.Comment;
import com.teamsalt.model.Task;
import com.teamsalt.model.User;

public class TaskController extends Controller {
	private static final Logger log = Logger.getLogger(TaskController.class);

	public void index() {
		renderText("TaskController is ok!");
	}

	/**
	 * 获取当前登录用户的任务
	 */
	@Before(GET.class)
	public void getMyTaskList() {
		String puuid = getPara(0);
		List list = Task.dao.getMyTaskList(puuid,
				ContextKit.getCurrentUserUuid(getRequest()));
		Map map = new HashMap();
		map.put("list", list);
		Message msg = new Message(0, "success", map);
		renderJson(msg);
	}

	/**
	 * 根据指定的任务id获取当前用户任务列表,
	 */
	@Before(GET.class)
	public void tasklistByTasklistUuid() {
		String pId = getPara("projectUuid");
		String tlId = getPara("tasklistUuid");
		List list = Task.dao.getTasksByTasklistUuid(pId, tlId);
		Map map = new HashMap();
		map.put("list", list);
		Message msg = new Message(0, "成功", map);
		renderJson(msg);
	}

	/**
	 * 得到userUuid下的tasks
	 */
	public void tasksByUserUuid() {
		String pId = getPara("projectUuid");
		String uId = getPara("userUuid");
		List list = Task.dao.getTasklistByUserUuid(pId, uId);
		Map map = new HashMap();
		map.put("list", list);
		Message msg = new Message(0, "成功", map);
		renderJson(msg);
	}

	@Before(GET.class)
	public void add() {
		String puuid = getPara(0);
		setAttr("puuid", puuid);
		render("/todo/add.html");
	}

	/**
	 * 整体更新
	 */
	@Before(POST.class)
	public void update() {
		Task task = getModel(Task.class);
		task.update();
		task = Task.dao.findById(task.get("uuid"));
		Map map = new HashMap();
		map.put("task", task);
		Message msg = new Message(0, "更新成功", map);
		renderJson(msg);
	}

	/**
	 * 按照提供的字段更新,单个更新
	 */
	@Before(POST.class)
	public void updateField() {
		String uuid = getPara("uuid");
		String field = getPara("field");
		String value = getPara("value");
		String type = getPara("type");// 字段类型

		Task task = new Task();
		task.set("uuid", uuid);
		task.set(field, value);
		task.update();
		task.set("assign_date",
				DateKit.getDateTime((Date) task.get("assign_date")));
		Map map = new HashMap();
		map.put("task", task);
		Message msg = new Message(0, "更新成功", map);
		renderJson(msg);
	}

	@Before(POST.class)
	public void addf() {
		Task task = getModel(Task.class);
		User user = ContextKit.getCurrentUser(getRequest());
		task.set("uuid", UuidKit.uuid());
		task.set("user_uuid", user.get("uuid"));
		task.set("creat_uuid", user.get("uuid"));
		task.set("create_time", DateKit.getNowDateTime());
		task.set("status", 0);
		if (task.getDate("assign_date") == null) {
			task.set("assign_date", null);
		}
		task.save();

		Map map = new HashMap();
		map.put("task", task);
		Message msg = new Message(0, "添加成功", map);

		renderJson(msg);

	}

	/*
	 * 详情
	 */
	@Before(GET.class)
	public void detail() {
		String tid = getPara(0);
		String pId = getPara(1);
		String tlId = getPara(2, "");
		Task task = Task.dao.getDetail(pId, tlId, tid);
		List list = Comment.dao.queryCommentsByTaskUuid(pId, tid);

		Map map = new HashMap();
		map.put("task", task);
		map.put("commentsList", list);
		Message msg = new Message(0, "完成", map);
		renderJson(msg);
	}

	/*
	 * 删除任务
	 */
	@Before(GET.class)
	public void delete() {
		String tuuid = getPara(0);
		Task task = new Task();
		task.deleteById(tuuid);
		Map map = new HashMap();
		map.put("uuid", tuuid);
		Message msg = new Message(0, "删除成功", map);
		renderJson(msg);
	}

	public void finish() {
		String todoUuid = getPara(0);
		Task task = new Task();
		task.updateFinish(todoUuid);
		task = Task.dao.findById(todoUuid);

		Map map = new HashMap();
		map.put("task", task);
		Message msg = new Message(0, "完成", map);

		renderJson(msg);
	}

	public void unfinish() {
		String todoUuid = getPara(0);
		Task task = new Task();
		task.updateUnfinish(todoUuid);
		task = Task.dao.findById(todoUuid);

		Map map = new HashMap();
		map.put("task", task);
		Message msg = new Message(0, "完成", map);

		renderJson(msg);
	}

}
