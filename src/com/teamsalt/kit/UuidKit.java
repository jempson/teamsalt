package com.teamsalt.kit;

import java.util.UUID;

/**
 * 
 * @ClassName: UuidKit
 * @Description: 生成类似5F5B7C1335954E3199445A44DA4DDD2D的UUID
 * @author zhaopeng
 * @date 2013-8-5 下午8:30:53
 * 
 */
public class UuidKit {
    public static String uuid() {
        return UUID.randomUUID().toString().replace("-", "").toUpperCase();
    }

    public static void main(String[] args) {
    }
}
