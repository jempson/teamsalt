package com.teamsalt.kit;

import org.apache.commons.codec.digest.DigestUtils;

public class PwdKit {
	private static final String SECRET = "1234567890";// 加密用的盐

	/**
	 * 加密密码
	 * 
	 * @param @return 设定文件
	 * @return String 返回类型
	 * @throws
	 */
	public static String pwdEncrypt(String pwd) {
		return DigestUtils.md5Hex(DigestUtils.md5Hex(pwd + SECRET));
	}
	public static void main(String[] args) {
		System.out.println(pwdEncrypt("1"));
	}
}
