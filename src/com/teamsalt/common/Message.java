package com.teamsalt.common;

import java.util.Map;

/**
 * 
 * @ClassName: Message
 * @Description: 返回结果信息
 * @author zhaopeng
 * @date 2013-8-10 上午12:23:50
 * 
 */
public class Message {
    private int code;// 0 成功;1 失败; -1 异常
    private String message;
    private Map other;

    public Message() {
    }

    public Message(int code, String message, Map other) {
        this.code = code;
        this.message = message;
        this.other = other;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Map getOther() {
        return other;
    }

    public void setOther(Map other) {
        this.other = other;
    }

}
