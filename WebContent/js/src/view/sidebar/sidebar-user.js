define(function(require, exports, module) {

    var $ = require('jquery');
    var _ = require('underscore');
    var Backbone = require('backbone');
    // 右键插件
    var Contextmenu = require('contextmenu');

    var ContextmenuView = require('../../ui-component/contextmenuView');

    // 左侧边栏
    var sidebarUserHtml = require('./tpl/sidebar-user.html');
    var userContextmenuHtml = require('./tpl/userContextmenu.html');

    var InviteUserWidget = require('../../widget/inviteUserWidget');

    var SidebarUser = Backbone.View.extend({
        el: '#sidebar-user',
        user:null,
        events: {
            'click li': 'switchUserTasklist',
            'click .invite-user':'inviteUser',
            'mousedown li':"userContextmenu"
        },
        initialize: function() {
            this.user =  Util.Models.get('user');
            this.user.getUsersByProject(
                {projectUuid:Util.curProjectId},
                $.proxy(this._loadUserList, this)
            );

        },
        userContextmenu:function(e){
            new ContextmenuView(e,userContextmenuHtml,function(item,curTarget){
                var operate = item.attr('data-operate');
                if('send'==operate){
                    new InviteUserWidget();
                }else if ('detail'==operate) {
                    new InviteUserWidget();
                } else if('del'==operate){
                    curTarget.remove();
                };
            });
        },
        inviteUser:function(e){
            e.preventDefault();
            e.stopPropagation();
            new InviteUserWidget(e,Util.curProjectId);
        },
        _loadUserList: function(userList) {
            userList.curProjectId=Util.curProjectId;
            this.$el.html(Util.tpl.t(sidebarUserHtml, userList)).hide().slideToggle('slow');
            this.$el.trigger('updateCommon');
        },
        switchUserTasklist: function(e) {//切换tasklist
            var curTasklist = $(e.currentTarget).find('a');
            if (curTasklist.hasClass('active')) {
                return;
            } else {
                $('#detail-panel').css({
                    width: '0%'
                });                
                $('#list-panel').css({
                    width: '100%'
                });                
                this._clearActiveTasklist();
                curTasklist.addClass('active');
                this.$el.trigger('updateCommon');
            }
        },
        _clearActiveTasklist: function() {//清除激活的tasklist
            $('.sidebar-menu>ul>li').each(function(index, val) {
                var activeTasklist = $(this).find('a');
                if (activeTasklist.hasClass('active')) {
                    activeTasklist.removeClass('active');
                    return;
                }
            });
        }

    });
    module.exports = SidebarUser;
})