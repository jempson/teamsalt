define(function(require, exports, module){

	var $ = require('jquery');
	var _ = require('underscore');
	var Backbone = require('backbone');

	var Project = Backbone.Model.extend({
		getProjectList: function(params, success, error) {
			Util.query.g('/project/getProjectsByMe','',function(resp){
				var curProject = null;
				
				var otherProjectlist = _.filter(resp.other.list,function(num){
					if(num.uuid != params){
						return num;
					}else{
						curProject = num;
					}
				});
				var results={
					curProject:curProject,
					otherProjectlist:otherProjectlist
				}
				success(results);
			});
		}
	});
	module.exports = Project;
})