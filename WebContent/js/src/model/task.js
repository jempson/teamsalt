define(function(require, exports, module) {

	var $ = require('jquery');
	var _ = require('underscore');
	var Backbone = require('backbone');

	var Task = Backbone.Model.extend({
		statusC:{//避免与mode中的status冲突,添加C表示常量
			noBegin:0,
			doing:1,
			end:2
		},
		saveNewTask : function(params, success) {
			var url = '/task/addf';
			Util.query.p(url, params, function(result) {
				success(result.other);
			});
		},
		update : function(params, success) {
			var url = '/task/update';
			Util.query.p(url, params, function(result) {
				result.other.task.assign_date = Util.date.getDate(result.other.task.assign_date);
				success(result.other);
			});
		},
		updateField : function(params, success) {//单个字段更新{field:field,value:value}
			var url = '/task/updateField';
			Util.query.p(url, params, function(result) {
				result.other.task.assign_date = Util.date.getDate(result.other.task.assign_date);
				success(result.other);
			});
		},
		getMyTaskList : function(params, success) {
			var url = '/task/getMyTaskList/' + params.projectid;
			var self = this;
			Util.query.g(url, '', function(result) {
				result.other.list = self._formatDate(result.other.list);
				success(result.other, params);
			});
		},
		/**
		 *  根据userUuid获取对应的tasklist
		 */
		getTasksByUserUuid : function(params, success, error) {
			var url = '/task/tasksByUserUuid';
			var self = this;
			Util.query.g(url, params, function(result) {
				result.other.list = self._formatDate(result.other.list);
				success(result.other, params);
			});
		},
		/**
		 *
		 * 根据tasklistId获取对应的taskList
		 */
		getTasksByTasklistUuid: function(params, success, error) {
			var url = '/task/tasklistByTasklistUuid';
			var self = this;
			Util.query.g(url, params, function(result) {
				result.other.list = self._formatDate(result.other.list);
				success(result.other, params);
			});
		},		
		/**
		 * 任务详情
		 */
		getTaskById : function(params, success, error) {
			var url = '/task/detail/' + params.taskId+'-'+params.projectId+'-'+params.tasklistId;
			var self = this;
			Util.query.g(url, '', function(result) {
				result.other.task.assign_date = Util.date.getDate(result.other.task.assign_date);
				success(result.other, params);
			});			
		},
		/**
		 * 更新为完成任务
		 */
		finish : function(taskId, success) {
			var url = '/task/finish/' + taskId;
			Util.query.g(url, '', function(result) {
				success(result.other, taskId);
			});			
		},
		/**
		 * 更新为未完成任务
		 */
		unfinish : function(taskId, success) {
			var url = '/task/unfinish/' + taskId;
			Util.query.g(url, '', function(result) {
				success(result.other, taskId);
			});			
		},
		/**
		 * 删除
		 */
		delete : function(taskId, success) {
			var url = '/task/delete/' + taskId;
			Util.query.g(url, '', function(result) {
				success(result.other, taskId);
			});			
		},
		filterByStatus : function(list,status) {//根据status过滤task
			var newList = _.filter(list,function (task) {
				if(task.status==status) 
					return task;
			});
			list = this._filterList(list, newList);
			return {
				list:list,
				newList:newList
			};
		},				
		/**
		 * 已经完成的任务,返回新list和剩余的list
		 */
		filterFinshedList : function(list) {
			var newList = _.filter(list,function (task) {
				if(task.status==1) 
					return task;
			});
			list = this._filterList(list, newList);
			return {
				list:list,
				newList:newList
			};
		},		
		/**
		 * 没有指定日期的任务,返回新list和剩余的list
		 */
		filterUnAssginDateList : function(list) {
			var newList = _.filter(list,function (task) {
				if(Util.kit.isNull(task.assign_date)) 
					return task;
			});
			list = this._filterList(list, newList);
			return {
				list:list,
				newList:newList
			};
		},		
		/**
		 * 取今天的任务,返回新list和剩余的list
		 */
		filterTodayList : function(list) {
			var todayDate = Util.date.nowDate();
			var newList = _.filter(list,function (task) {
				if(todayDate==task.assign_date) 
					return task;
			});
			list = this._filterList(list, newList);
			return {
				list:list,
				newList:newList
			};			
		},		
		/**
		 * 按任务时间排序,desc true:倒序 false:正序
		 */
		filterSortByDate : function(list,desc) {
			list = _.sortBy(list, function(task) {
				return task.assign_date;
			});	
			if(!desc){
				list = list.reverse();
			}
			return list;
		},		
		//格式化日期,去除时间
		_formatDate:function (list) {
			if(!Util.kit.isNull(list)){
				for (var i = 0; i < list.length; i++) {
					var t = list[i];
					t.assign_date = Util.date.getDate(t.assign_date);
					list[i]=t;
				};
			}
			return list;
		},
		/*
		* 将filterList从list中过滤掉
		*/
		_filterList:function (list,filterList) {
			var result = new Array();
			for (var i = 0,l = list.length; i < l; i++) {
				var v = list[i];
				var flag = false;
				for (var j = 0,len = filterList.length; j < len; j++) {
					var val = filterList[j];
					if(v.uuid == val.uuid) {
						flag = true;						
						break;
					}
				};
				if(!flag) result.push(v);
			};
			return result;
		}
	});
	module.exports = Task;
})