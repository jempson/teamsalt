define(function(require, exports, module){

	var $ = require('jquery');
	var _ = require('underscore');
	var Backbone = require('backbone');


	//使用artTemplate模板引擎
	var DropdownView = require('../ui-component/dropdownView');

    // 指定分配人模板
    var taskPriorityHtml = require('./tpl/task-priority.html');


	var AaskPriorityWidget = function(curTarget,taskId){
		this.curTarget = curTarget;
		this.init();
	}
	AaskPriorityWidget.prototype.init = function() {
		this.aaskPriorityDropdownView = new DropdownView(this.curTarget,Util.tpl.t(taskPriorityHtml, {}));
        this.aaskPriorityDropdownView.open();
        this.aaskPriorityDropdownView.getRoot().find('li>a').on('click', $.proxy(this.selectUser, this));
	};
	AaskPriorityWidget.prototype.selectUser = function(e) {
		var curPriority = $(e.currentTarget).text();

		seajs.log(this.curTarget.val(curPriority));
	};
	
	module.exports = AaskPriorityWidget;
})