module.exports = function (grunt) {
    var transport = require('grunt-cmd-transport');
    var style = transport.style.init(grunt);
    var text = transport.text.init(grunt);
    var script = transport.script.init(grunt);

    grunt.initConfig({
        pkg : grunt.file.readJSON("package.json"),

        transport : {
            options : {
                paths : ['.'],
                alias: '<%= pkg.spm.alias %>',
                parsers : {
                    '.js' : [script.jsParser],
                    '.css' : [style.css2jsParser],
                    '.html' : [text.html2jsParser]
                }
            },
            app : {
                options : {
                    idleading : 'app/'
                },
                files : [
                    {
                        cwd : 'src',
                        src : '**/*',
                        filter : 'isFile',
                        dest : '.build'
                    }
                ]
            }
        },
        concat : {
            options : {
                paths : ['.']
            },
            app : {
                options : {
                    include : 'all'
                },
                files: [
                    {
                        expand: true,
                        cwd: '.build/',
                        src: ['**/*.js'],
                        dest: 'dist/',
                        ext: '.js'
                    }
                ]
            }
        },

        uglify : {
            app : {
                files: [
                    {
                        expand: true,
                        cwd: 'dist/',
                        src: ['**/*.js', '!**/*-debug.js'],
                        dest: 'app/',
                        ext: '.js'
                    }
                ]
            },
            appmin : {
                src: ['seajs/seajs/2.1.1/sea.js','config.js','app/app.js'],
                dest: 'app/appmin.js'
            }            
        },

        clean : {
            spm : ['.build','app','dist']
        }
    });

    grunt.loadNpmTasks('grunt-cmd-transport');
    grunt.loadNpmTasks('grunt-cmd-concat');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-uglify');

    grunt.registerTask('build-t', ['transport']);
    grunt.registerTask('build-c', ['concat']);
    grunt.registerTask('build-u', ['uglify']);
    grunt.registerTask('build-min', ['uglify:appmin']);
    grunt.registerTask('build-all', ['transport','concat','uglify']);

    grunt.registerTask('default', ['clean']);

};