module.exports = function(grunt) {
    // 配置
    grunt.initConfig({
        pkg : grunt.file.readJSON('package.json'),
        concat : {
            css : {
                src: ['src/bootstrap.css','src/font-awesome.min.css','src/perfect-scrollbar.css','src/datepicker.css','src/select2.css','src/bootstrap-tags.css','src/popbox.css','src/jquery.dropdown.css','src/bootstrap-notify.css','src/style.css'],
                dest: '.build/all.css'
            }
        },
        cssmin: {
            css: {
                src: '.build/all.css',
                dest: 'style/all-min.css'
            }
        },
        clean : {
            spm : ['.build']
        }
    });
    // 载入concat和css插件，分别对于合并和压缩
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-css');
    // 默认任务
    grunt.registerTask('default', ['concat', 'cssmin','clean']);
};